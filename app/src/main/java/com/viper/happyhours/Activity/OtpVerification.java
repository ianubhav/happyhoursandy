package com.viper.happyhours.Activity;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.gc.materialdesign.views.ButtonFlat;
import com.viper.happyhours.R;
import com.viper.happyhours.Utils.ApiRequests;
import com.viper.happyhours.Utils.Config;
import com.viper.happyhours.Utils.Extras;
import com.viper.happyhours.Utils.RealmTasks;
import com.viper.happyhours.Utils.VolleyResponseListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class OtpVerification extends AppCompatActivity{
    private String mobileNo,token;
    private String TAG = getClass().getName();
    private EditText otpET;
    private ButtonFlat resend;
    private ButtonFlat submit;
    private Map<String, String> paramsResend = new HashMap<String, String>();
    private  Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("Privatelog", "OtpVerification: onCreate");
        setContentView(R.layout.activity_otp_verification);
        final Intent intent = getIntent();
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        toolbar = (Toolbar) findViewById(R.id.toolbar_otp_verification);
        toolbar.setTitle("OTP VERIFICATION");
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_18dp);
        mobileNo = intent.getExtras().getString("mobileNo");
        token = intent.getExtras().getString("token");
        otpET = (EditText) findViewById(R.id.otp_et);
        resend = (ButtonFlat) findViewById(R.id.otp_resend);
        submit = (ButtonFlat) findViewById(R.id.otp_submit);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
//                overridePendingTransition(R.transition.exit, R.transition.enter);
            }
        });
        resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pDialog.show();
                paramsResend.put("mobile_no", mobileNo);
                ApiRequests.makeJsonObjectRequest(Request.Method.POST,getBaseContext(), Config.userLogin, paramsResend, new VolleyResponseListener() {
                    @Override
                    public void onError(String message) {
                        pDialog.dismiss();
                    }
                    @Override
                    public void onResponse(Object response) {
                        JSONObject jsonData = (JSONObject) response;
                        try {
                            pDialog.dismiss();
                            token = jsonData.getString("token");
                            Toast.makeText(getBaseContext(), jsonData.getString("message"), Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Map<String, String> params = new HashMap<String, String>();
                params.put("otp", otpET.getText().toString());
                params.put("mobile_no", mobileNo);
                params.put("token", token);
                pDialog.show();
                ApiRequests.makeJsonObjectRequest(Request.Method.POST,getBaseContext(), Config.userOtpVerifyUrl, params, new VolleyResponseListener() {
                    @Override
                    public void onError(String message) {
                        Extras.internetConnectionToast(getBaseContext(),message);
                        pDialog.dismiss();
                    }

                    @Override
                    public void onResponse(Object response) {
                        JSONObject jsonData = (JSONObject) response;
                        pDialog.dismiss();
                        try {
                            if(jsonData.getBoolean("is_otp_verified") && !jsonData.getBoolean("is_error")) {
                                RealmTasks.updateUserObject(jsonData,getBaseContext());
                                Intent i = new Intent(OtpVerification.this, CheckLocationActivity.class);
                                startActivity(i);
//                                overridePendingTransition(R.transition.enter, R.transition.exit);
                            }
                            Toast.makeText(getBaseContext(), jsonData.getString("message"), Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                });
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("Privatelog", "OtpVerification: onResume");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("Privatelog", "OtpVerification: onRestart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("Privatelog", "OtpVerification: onStop");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("Privatelog", "OtpVerification: onPause");
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("Privatelog", "OtpVerification: onDestroy");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        overridePendingTransition(R.transition.exit, R.transition.enter);
    }
}
