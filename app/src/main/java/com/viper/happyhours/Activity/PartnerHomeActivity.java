package com.viper.happyhours.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.github.ivbaranov.mfb.MaterialFavoriteButton;
import com.viper.happyhours.Adapters.ListViewAdapterOffers;
import com.viper.happyhours.Adapters.ListViewAdapterOthers;
import com.viper.happyhours.DataModels.OffersList;
import com.viper.happyhours.Fragment.BookingDialogFragment;
import com.viper.happyhours.MainApplication;
import com.viper.happyhours.R;
import com.viper.happyhours.Utils.ApiRequests;
import com.viper.happyhours.Utils.Config;
import com.viper.happyhours.Utils.RealmTasks;
import com.viper.happyhours.Utils.VolleyResponseListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.techery.properratingbar.ProperRatingBar;

public class PartnerHomeActivity extends AppCompatActivity {
    private ProperRatingBar rating;
    private NetworkImageView main_img;
    private ImageView share;
    private ImageView comment;
    private MaterialFavoriteButton favourite;
    private ListView offers;
    private ListView others;
    private Toolbar toolbar;
    private ImageLoader imageLoader;
    private Map<String, String> params = new HashMap<String, String>();
    private int partnerId = 1;
    public static int offerId;
    public static ArrayList<Integer> otherIds= new ArrayList<Integer>();
    public static ArrayList<ArrayList<OffersList>> arrayList = new ArrayList<ArrayList<OffersList>>();
//    public static String timing = "Any time";
    public static String title ="Title";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imageLoader = MainApplication.getInstance().getImageLoader();
        arrayList.clear();
        offerId = -1;
        otherIds.clear();
        setContentView(R.layout.activity_partner_home);
        Intent i = getIntent();
        partnerId = i.getExtras().getInt("partnerId");
        main_img = (NetworkImageView) findViewById(R.id.partner_iv);
        toolbar = (Toolbar) findViewById(R.id.partner_homepage_toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_18dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
//                overridePendingTransition(R.transition.exit, R.transition.enter);
            }
        });
        rating = (ProperRatingBar) findViewById(R.id.partner_homepage_ratings);
        offers = (ListView) findViewById(R.id.partner_homepage_offers_listview);
        others = (ListView) findViewById(R.id.partner_homepage_others_listview);
        favourite = (MaterialFavoriteButton) findViewById(R.id.partner_homepage_favorite);
        params.put("id", String.valueOf(partnerId));
        params.put("token",RealmTasks.getToken());
        params.put("date", Config.dateFormat.format(RealmTasks.getSelectedDate()));
        ApiRequests.makeJsonObjectRequest(Request.Method.POST, getBaseContext(), Config.partnerHome, params, new VolleyResponseListener() {
            @Override
            public void onError(String message) {
            }

            @Override
            public void onResponse(Object response) {
                JSONObject jsonData = (JSONObject) response;
                arrayList = setServicesList(jsonData);
                final ListViewAdapterOffers offersadapter = new ListViewAdapterOffers(getBaseContext(),R.layout.listview_offers,arrayList.get(0));
                final ListViewAdapterOthers othersadapter = new ListViewAdapterOthers(getBaseContext(),R.layout.listview_others,arrayList.get(1));
                offers.setAdapter(offersadapter);
                others.setAdapter(othersadapter);
                ListUtils.setDynamicHeight(offers);
                ListUtils.setDynamicHeight(others);
                offers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        if(position==offersadapter.getSelected()) {
                            offersadapter.setSelected(-1);
                        }
                        else {
                            offersadapter.setSelected(position);
                        }
//                        timing = offersadapter.getExactTime();
                        offerId = offersadapter.getSelected();
                        offersadapter.notifyDataSetChanged();
                    }
                });
                others.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        if(othersadapter.containsSelected(position)){
                            othersadapter.removeSelected(position);
                        }
                        else {
                            othersadapter.addSelected(position);
                        }
                        otherIds = othersadapter.getArrayList();
                        othersadapter.notifyDataSetChanged();
                        Log.d("Privatelog",othersadapter.getArrayList().toString());
                    }
                });
            }
        });
    }

    private ArrayList<ArrayList<OffersList>> setServicesList(final JSONObject jsondata){
        ArrayList<ArrayList<OffersList>> arrayList = new ArrayList<ArrayList<OffersList>>();
        ArrayList<OffersList> offersLists = new ArrayList<OffersList>();
        ArrayList<OffersList> otherslist = new ArrayList<OffersList>();
        JSONArray data = null;
        try {
            title = jsondata.getJSONObject("data").getString("businessName");
            main_img.setImageUrl(jsondata.getJSONObject("data").getString("partnerImgUrl"),imageLoader);
            toolbar.setTitle(title);
            favourite.setFavorite(RealmTasks.getFavourite(jsondata.getJSONObject("data").getInt("id")));
            favourite.setOnFavoriteChangeListener(
                    new MaterialFavoriteButton.OnFavoriteChangeListener() {
                        @Override
                        public void onFavoriteChanged(MaterialFavoriteButton buttonView, boolean favorite) {
                            try {
                                RealmTasks.setFavourite(jsondata.getJSONObject("data").getInt("id"));
                                Log.d("Privatelog", String.valueOf(RealmTasks.getFavourite(jsondata.getJSONObject("data").getInt("id"))));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
            toolbar.setSubtitle(jsondata.getJSONObject("data").getString("tagLine"));
            data = jsondata.getJSONArray("services");
            for (int i = 0; i < data.length(); i++) {
                try {
                    JSONObject row = data.getJSONObject(i);
                    OffersList cardItem = new OffersList();
                    cardItem.setId(row.getInt("id"));
                    cardItem.setService_name(row.getString("serviceName"));
                    cardItem.setActualFees(row.getInt("fees"));
                    cardItem.setDiscountFees(row.getInt("discountFees"));
                    cardItem.setPercent(row.getInt("discountPercentage"));
                    cardItem.setTimings(row.getJSONArray("dateTime"));
                    cardItem.setOfferType(row.getString("serviceType"));
                    if(cardItem.getOfferType().equals("offer")){
                        offersLists.add(cardItem);
                    }
                    else {
                        otherslist.add(cardItem);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();

        }
        arrayList.add(offersLists);
        arrayList.add(otherslist);
        return arrayList;
    }

    public static class ListUtils {
        public static void setDynamicHeight(ListView mListView) {
            ListAdapter mListAdapter = mListView.getAdapter();
            if (mListAdapter == null)
                return;
            int height = 0;
            int desiredWidth = View.MeasureSpec.makeMeasureSpec(mListView.getWidth(), View.MeasureSpec.UNSPECIFIED);
            for (int i = 0; i < mListAdapter.getCount(); i++) {
                View listItem = mListAdapter.getView(i, null, mListView);
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                height += listItem.getMeasuredHeight();
            }
            ViewGroup.LayoutParams params = mListView.getLayoutParams();
            params.height = height + (mListView.getDividerHeight() * (mListAdapter.getCount() - 1));
            mListView.setLayoutParams(params); mListView.requestLayout();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.booking_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.booking_icon) {
            Log.d("Privatelog", offerId + "                " + otherIds.toString());
//            if()
            showBookingDialog();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showBookingDialog() {
        if (offerId > -1 | otherIds.size() > 0) {
            FragmentManager fm = getSupportFragmentManager();
            BookingDialogFragment dialogFragment = BookingDialogFragment.newInstance();
            dialogFragment.show(fm, "booking_dialog");
        }
        else {
            Toast.makeText(getBaseContext(),"Please Select atleast one service",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
