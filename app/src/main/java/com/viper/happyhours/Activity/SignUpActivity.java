package com.viper.happyhours.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.gc.materialdesign.views.ButtonRectangle;
import com.viper.happyhours.R;
import com.viper.happyhours.Utils.ApiRequests;
import com.viper.happyhours.Utils.Config;
import com.viper.happyhours.Utils.Extras;
import com.viper.happyhours.Utils.VolleyResponseListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SignUpActivity extends AppCompatActivity {
    private ButtonRectangle mLogin;
    private EditText mobileNo;
    private EditText referralCode;
    private Map<String, String> params = new HashMap<String, String>();
    private Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        final ProgressDialog pDialog = new ProgressDialog(this);
        mobileNo = (EditText) findViewById(R.id.signup_screen_mobile_no);
        mLogin = (ButtonRectangle) findViewById(R.id.signup_screen_login_btn);
        toolbar = (Toolbar) findViewById(R.id.toolbar_signup);
        toolbar.setTitle("Sign Up");
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_18dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
//                overridePendingTransition(R.transition.exit, R.transition.enter);
            }
        });
        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mobileNoString = mobileNo.getText().toString();
                if (Extras.isValidMobile(mobileNoString)) {
                    final String finalMobileNo = Extras.removeZeroMobileNo(mobileNo.getText().toString());
                    params.put("mobile_no", finalMobileNo);
                    pDialog.show();
                    ApiRequests.makeJsonObjectRequest(Request.Method.POST, getBaseContext(), Config.signUpUserUrl, params, new VolleyResponseListener() {
                        @Override
                        public void onError(String message) {
                            Extras.internetConnectionToast(getBaseContext(), message);
                            pDialog.dismiss();
                        }

                        @Override
                        public void onResponse(Object response) {
                            JSONObject jsonData = (JSONObject) response;
                            try {
                                if (!jsonData.getBoolean("is_error")) {
                                    if(jsonData.getBoolean("user_exists")){

                                    }
                                    else {
                                        String token = jsonData.getString("token");
                                        Intent i = new Intent(SignUpActivity.this, OtpVerificationSignup.class);
                                        i.putExtra("mobileNo", finalMobileNo);
                                        i.putExtra("token", token);
                                        startActivity(i);
//                                    overridePendingTransition(R.transition.enter, R.transition.exit);
                                    }
                                }
                                Toast.makeText(getBaseContext(), jsonData.getString("message"), Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                Extras.jsonError(getBaseContext());
                                e.printStackTrace();
                            }

                        }
                    });
                } else {
                    Toast.makeText(getBaseContext(), getString(R.string.mobileNoError), Toast.LENGTH_SHORT).show();
                }
                pDialog.dismiss();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("Privatelog", "SignUpActivity: onResume");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("Privatelog", "SignUpActivity: onRestart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("Privatelog", "SignUpActivity: onStop");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("Privatelog", "SignUpActivity: onPause");
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("Privatelog", "SignUpActivity: onDestroy");
    }
}
