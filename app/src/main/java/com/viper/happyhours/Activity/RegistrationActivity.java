package com.viper.happyhours.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.viper.happyhours.R;
import com.viper.happyhours.Utils.ApiRequests;
import com.viper.happyhours.Utils.Config;
import com.viper.happyhours.Utils.Extras;
import com.viper.happyhours.Utils.RealmTasks;
import com.viper.happyhours.Utils.VolleyResponseListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegistrationActivity extends AppCompatActivity {
    private Button submit;
    private EditText firstname;
    private EditText lastname;
    private Map<String, String> params = new HashMap<String, String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("Privatelog", "RegistrationActivity: onCreate");
        setContentView(R.layout.activity_registration);
        final String android_id = Settings.Secure.getString(getBaseContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        final String version = Build.VERSION.RELEASE;
        final ProgressDialog pDialog = new ProgressDialog(this,R.style.CustomTheme);
        firstname = (EditText) findViewById(R.id.reg_first_name);
        lastname = (EditText) findViewById(R.id.reg_last_name);
        submit = (Button) findViewById(R.id.reg_submit);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (firstname.getText().toString().length() > 0) {
                    params.put("token", RealmTasks.getToken());
                    params.put("first_name", firstname.getText().toString());
                    params.put("os_version", version);
                    params.put("os_type", "android");
                    params.put("device_id", android_id);
                    if (firstname.getText().toString().length() > 0)
                        params.put("last_name", lastname.getText().toString());
                    pDialog.show();
                    ApiRequests.makeJsonObjectRequest(Request.Method.POST,getBaseContext(), Config.updateUserUrl, params, new VolleyResponseListener() {
                        @Override
                        public void onError(String message) {
                            Extras.internetConnectionToast(getBaseContext(),message);
                            pDialog.dismiss();
                        }

                        @Override
                        public void onResponse(Object response) {
                            JSONObject jsonData = (JSONObject) response;
                            try {
                                if (!jsonData.getBoolean("is_error")) {
                                    Intent i = new Intent(RegistrationActivity.this, InitialLocationActivity.class);
                                    startActivity(i);
                                }
                            } catch (JSONException e) {
                                Extras.jsonError(getBaseContext());
                                e.printStackTrace();
                            }
                            pDialog.dismiss();
                        }
                    });
                } else {
                    Toast.makeText(getBaseContext(), "Please Enter Your First Name", Toast.LENGTH_SHORT).show();
                }
                pDialog.dismiss();
            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
        Log.d("Privatelog", "RegistrationActivity: onResume");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("Privatelog", "RegistrationActivity: onRestart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("Privatelog", "RegistrationActivity: onStop");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("Privatelog", "RegistrationActivity: onPause");
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("Privatelog", "onDestroy: ");
        Log.d("Privatelog", "RegistrationActivity: onDestroy");
    }
}
