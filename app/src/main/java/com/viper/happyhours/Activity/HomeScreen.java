package com.viper.happyhours.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.viper.happyhours.Adapters.HomeScreenNavigationAdapter;
import com.viper.happyhours.DataModels.NavDrawerItem;
import com.viper.happyhours.Fragment.BookingFragment;
import com.viper.happyhours.Fragment.HomeFragment;
import com.viper.happyhours.R;
import com.viper.happyhours.Utils.Config;
import com.viper.happyhours.Utils.RealmTasks;

import java.util.ArrayList;
import java.util.Date;


public class HomeScreen extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ArrayList<NavDrawerItem> navDrawerItems;
    private HomeScreenNavigationAdapter adapter;
    private String TAG = "Home_Screen";
    private Toolbar toolbar;
    private LinearLayout toolbar_normal_linLayout;
    private TextView location_tv;
    private TextView phone_no;
    private TextView logout;
    private Fragment fragment;
    private TextView toolbar_display_tv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RealmTasks.setSelectedDate(new Date());
        setContentView(R.layout.activity_home_screen);
        toolbar = (Toolbar) findViewById(R.id.homescreen_toolbar);
        toolbar_normal_linLayout = (LinearLayout) findViewById(R.id.toolbar_normal_fragment_lin_layout);
        toolbar_display_tv = (TextView) findViewById(R.id.toolbar_other_text);
        phone_no = (TextView) findViewById(R.id.sidemenu_phone_no);
        mDrawerList = (ListView) findViewById(R.id.sidemenu_listview);
        location_tv = (TextView) findViewById(R.id.toolbar_location_text);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.homescreen_drawer_layout);
        navDrawerItems = Config.sideMenuTabs;
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        location_tv.setText(RealmTasks.getLocationText());
        phone_no.setText(RealmTasks.getMobileNo());
        adapter = new HomeScreenNavigationAdapter(getApplicationContext(), navDrawerItems);
        Fragment fragment = new HomeFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.homescreen_frame_container, fragment).commit();
        mDrawerList.setAdapter(adapter);
        toolbar_normal_linLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeScreen.this, CheckLocationActivity.class);
                startActivity(i);
                Log.d("Privatelog", "HomeScreen: onClick");
            }
        });
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(position == 0 ){
                    toolbar_display_tv.setVisibility(View.GONE);
                    toolbar_normal_linLayout.setVisibility(View.VISIBLE);
                }
                else {
                    toolbar_display_tv.setVisibility(View.VISIBLE);
                    toolbar_normal_linLayout.setVisibility(View.GONE);
                    toolbar_display_tv.setText(navDrawerItems.get(position).getTitle());
                }

                Fragment fragment = new HomeFragment();
                switch (position){
                    case 0:
                        Log.d("Privatelog1", "HomeScreen: onItemClick case 0");
                        fragment = new HomeFragment();
                        break;
                    case 1:
                        Log.d("Privatelog1", "HomeScreen: onItemClick case 1");
                        fragment = new BookingFragment();
                        break;
                    case 2:
                        Log.d("Privatelog1", "HomeScreen: onItemClick case 2");
                        RealmTasks.logoutUser(getApplicationContext());
                        break;
                }
                if(position!=2) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.homescreen_frame_container, fragment).commit();
                    mDrawerLayout.closeDrawers();
                }
            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.homescreen_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.homescreen_sidelist_btn) {
            mDrawerLayout.openDrawer(Gravity.RIGHT);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
