package com.viper.happyhours.Activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.lsjwzh.widget.materialloadingprogressbar.CircleProgressBar;
import com.viper.happyhours.R;
import com.viper.happyhours.Utils.RealmTasks;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class InitialLocationActivity extends AppCompatActivity {
    private LocationManager locationManager = null;
    private LocationListener locationListener = null;
    private CircleProgressBar pb = null;

    private static final String TAG = "Debug";
    private Boolean flag = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("Privatelog", "InitialLocationActivity: onCreate");
        setContentView(R.layout.activity_intial_location);
        //if you want to lock screen for always Portrait mode
        pb = (CircleProgressBar) findViewById(R.id.initial_loc_pb);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

    }

    @Override
    protected void onResume() {
        super.onResume();
        pb.setVisibility(View.INVISIBLE);
        flag = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (flag) {
            onGPSEnabled(getBaseContext());
        } else {
            alertbox("Gps Status!!", "Your GPS is: OFF");
        }
    }

    /*----------Method to create an AlertBox ------------- */
    protected void alertbox(String title, String mymessage) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your Device's GPS is Disable")
                .setCancelable(false)
                .setTitle("** Gps Status **")
                .setPositiveButton("Gps On",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // finish the current activity
                                // AlertBoxAdvance.this.finish();
                                Intent myIntent = new Intent(
                                        Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(myIntent);
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.setCancelable(false);
        alert.show();
    }

    /*----------Listener class to get coordinates ------------- */
    private class MyLocationListener implements LocationListener {
        @Override
        public void onLocationChanged(Location loc) {
            if (ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locationManager.removeUpdates(locationListener);
            pb.setVisibility(View.INVISIBLE);
            Toast.makeText(getBaseContext(),"Location changed : Lat: " + loc.getLatitude()+ " Lng: " + loc.getLongitude(), Toast.LENGTH_SHORT).show();
            Geocoder gcd = new Geocoder(getBaseContext(), Locale.getDefault());
            List<Address> addresses = null;
            try {
                addresses = gcd.getFromLocation(loc.getLatitude(), loc.getLongitude(), 1);
                Log.d(TAG, "onLocationChanged: " + addresses.get(0).getLocality());
                Double longitude = loc.getLongitude();
                Double latitude = loc.getLatitude();


            } catch (IOException e) {
                e.printStackTrace();
            }
//            setLocationAndStartActivity(loc.getLatitude(), loc.getLongitude(), addresses.get(0).getLocality());
            setLocationAndStartActivity(loc.getLatitude(), loc.getLongitude(), "Chandigarh");
        }

        @Override
        public void onProviderDisabled(String provider) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onStatusChanged(String provider,
                                    int status, Bundle extras) {
            // TODO Auto-generated method stub
        }
    }
    public void onGPSEnabled(Context context){
        pb.setVisibility(View.VISIBLE);
        locationListener = new MyLocationListener();
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager
                .NETWORK_PROVIDER, 5000, 100, locationListener);
    }
    private void setLocationAndStartActivity(Double lat, Double lng,String locationText){
        RealmTasks.setLocationInUserObject(lat,lng,locationText,getBaseContext(),false);
        Intent i = new Intent(InitialLocationActivity.this, HomeScreen.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finish();
    }
    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("Privatelog", "onRestart: ");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("Privatelog", "onStop: ");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("Privatelog", "onPause: ");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("Privatelog", "onDestroy: ");
    }
}
