package com.viper.happyhours.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.viper.happyhours.Fragment.ViewPagerFragment;
import com.viper.happyhours.Utils.Config;

/**
 * Created by Viper on 15/03/16.
 */
public class HomeScreenViewPagerAdapter extends FragmentPagerAdapter {
    private int mNumOfTabs;
    public static String[] homeScreenTabs = Config.homeScreenTabs;
    public HomeScreenViewPagerAdapter(FragmentManager fm,int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int index) {
//        return HomeFragment.newInstance(homeScreenTabs[index]);
        return ViewPagerFragment.newInstance(homeScreenTabs[index]);
    }

    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return mNumOfTabs;
    }

}
