package com.viper.happyhours.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.viper.happyhours.DataModels.ShowBookingItem;
import com.viper.happyhours.R;

import java.util.ArrayList;

/**
 * Created by Viper on 22/04/16.
 */
public class ShowBookingAdapter extends RecyclerView.Adapter<ShowBookingAdapter.MyViewHolder> {
    ArrayList<ShowBookingItem> bookingList;
    public ShowBookingAdapter (ArrayList<ShowBookingItem> bookingList){
        this.bookingList = bookingList;
    }

    @Override
    public int getItemCount() {
        return bookingList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView bookingId,bookingTime,actualTime,partnerName;
        public MyViewHolder(View view) {
            super(view);
            bookingId = (TextView) view.findViewById(R.id.show_booking_number);
            bookingTime = (TextView) view.findViewById(R.id.show_booking_made_time);
            actualTime = (TextView) view.findViewById(R.id.show_booking_time);
            partnerName = (TextView) view.findViewById(R.id.show_booking_partner_name);
        }

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.booking_card, viewGroup, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.bookingTime.setText(bookingList.get(position).getBookingTime());
        holder.partnerName.setText(bookingList.get(position).getPartner_name());
        holder.actualTime.setText(bookingList.get(position).getBookingActualTime());
        holder.bookingId.setText("Booking No: "+bookingList.get(position).getId());
    }
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
