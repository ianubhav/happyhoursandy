package com.viper.happyhours.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.viper.happyhours.DataModels.OffersList;
import com.viper.happyhours.R;

import java.util.ArrayList;

/**
 * Created by Viper on 06/04/16.
 */
public class BookingListAdapter extends ArrayAdapter<OffersList> {

    public BookingListAdapter(Context context, int resource, ArrayList<OffersList> items) {
        super(context, resource, items);
        Log.d("Privatelogjk", "come here also");

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.booking_list_item, null);
        }

        OffersList p = getItem(position);

        if (p != null) {
            TextView actualPrice = (TextView) v.findViewById(R.id.listview_booking_actual_price);
            TextView serviceName = (TextView) v.findViewById(R.id.listview_booking_label);
            serviceName.setText(p.getService_name());
            actualPrice.setText(""+p.getDiscountFees());
        }

        return v;
    }

}
