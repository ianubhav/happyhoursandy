package com.viper.happyhours.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.widget.AppCompatRadioButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.viper.happyhours.DataModels.OffersList;
import com.viper.happyhours.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Viper on 06/04/16.
 */
public class ListViewAdapterOffers extends ArrayAdapter<OffersList> {
    int selectedIndex = -1;

    public ListViewAdapterOffers(Context context, int resource, ArrayList<OffersList> items) {
        super(context, resource, items);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.listview_offers, null);
        }

        OffersList p = getItem(position);

        if (p != null) {
            RelativeLayout rl = (RelativeLayout) v.findViewById(R.id.listview_offers_layout);
            AppCompatRadioButton rb = (AppCompatRadioButton) v.findViewById(R.id.radioButton_offers);
            TextView actualPrice = (TextView) v.findViewById(R.id.listview_offers__actual_price);
            TextView discountPercent = (TextView) v.findViewById(R.id.listview_offers__discount_percent);
            TextView offerName = (TextView) v.findViewById(R.id.listview_offers_label);
            TextView discountPrice = (TextView) v.findViewById(R.id.listview_offers__discount_price);
            TextView time = (TextView) v.findViewById(R.id.listview_offers_timing);
            if(position != selectedIndex) {
                rb.setChecked(false);
                rl.setBackgroundColor(Color.parseColor("#ffffff"));
            }
            else {
                rb.setChecked(true);
                rl.setBackgroundColor(Color.parseColor("#DFDEDE"));
            }
            actualPrice.setText("Rs "+p.getActualFees());
            discountPercent.setText(String.valueOf((int)p.getPercent() + "%"));
            offerName.setText(p.getService_name());
            discountPrice.setText("Rs "+p.getDiscountFees());
            time.setText(getTimings(p.getTimings()));
            actualPrice.setPaintFlags(actualPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }

        return v;
    }

    public void setSelected(int selected) {
        this.selectedIndex = selected;
    }
    public int getSelected() {
        return selectedIndex;
    }
    public String getExactTime() {
        if(getSelected()==-1){
            return "Any time";
        }
        JSONArray time= getItem(getSelected()).getTimings();
        return getTimings(time);
    }
    public int getSelectedId(int position) {
        if(position ==-1){
            return -1;
        }
        return getItem(position).getId();
    }
    private String getTimings(JSONArray timings){
        Log.d("Privatelog", timings.toString());
        String time = "";
        try {
            for (int i=0;i<timings.length();i++) {
                time = time + timings.get(i)+" ";
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return time;
    }
}
