package com.viper.happyhours.Adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.github.ivbaranov.mfb.MaterialFavoriteButton;
import com.viper.happyhours.DataModels.HomeScreenCardItem;
import com.viper.happyhours.MainApplication;
import com.viper.happyhours.R;

import java.util.ArrayList;

/**
 * Created by Viper on 19/03/16.
 */
public class CardViewAdapter extends RecyclerView.Adapter<CardViewAdapter.MyViewHolder> {
    ArrayList<HomeScreenCardItem> cardItemArrayList;
    private OnItemClickListener listener;
    public CardViewAdapter (ArrayList<HomeScreenCardItem> cardItemArrayList){
        this.cardItemArrayList = cardItemArrayList;
    }

    @Override
    public int getItemCount() {
        return cardItemArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView discount, title, tagline, timings;
        public MaterialFavoriteButton  favorite;
        public NetworkImageView imageView;
        public CardView cardView;
        private OnItemClickListener listener;

        public MyViewHolder(View view) {
            super(view);
            cardView = (CardView) view.findViewById(R.id.cv_homescreen);
            title = (TextView) view.findViewById(R.id.cv_homescreen_title);
            tagline = (TextView) view.findViewById(R.id.cv_homescreen_tagline);
            timings = (TextView) view.findViewById(R.id.cv_homescreen_timings);
            discount = (TextView) view.findViewById(R.id.cv_homescreen_discount);
            imageView = (NetworkImageView) view.findViewById(R.id.cv_homescreen_iv);
            favorite = (MaterialFavoriteButton) view.findViewById(R.id.cv_homescreen_favorite);
        }

        @Override
        public void onClick(View v) {

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cardview_homescreen, viewGroup, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        ImageLoader imageLoader = MainApplication.getInstance().getImageLoader();
        holder.title.setText(cardItemArrayList.get(position).getPartner_name());
        int discount = cardItemArrayList.get(position).getMax_discount();
        if(discount>0)
            holder.discount.setText(discount+"%");
        holder.tagline.setText(cardItemArrayList.get(position).getTagline());
        holder.timings.setText(cardItemArrayList.get(position).getTimings());
        holder.favorite.setType(MaterialFavoriteButton.STYLE_HEART);
        holder.imageView.setImageUrl(cardItemArrayList.get(position).getPartner_img(),imageLoader);
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null)
                    listener.onItemClick(cardItemArrayList.get(position).getId());
            }
        });
    }
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
    public void setOnItemClickListener(OnItemClickListener listener){
        this.listener = listener;
    }

    public interface OnItemClickListener{
        public void onItemClick(int id);
    }
//    private String setShortTime(Date start,Date finish){
//        DateFormat startFormat = new SimpleDateFormat("h");
//        DateFormat endFormat = new SimpleDateFormat("ha");
//        String finalText="";
//        finalText= startFormat.format(start)+"-"+endFormat.format(finish);
//        return finalText;
//    }
//    private String getTimings(JSONArray timings){
//        if(timings.length()==0){
//            return "Happy Hrs: Not yet Check again";
//        }
//        else {
//            String time = "Happy Hrs: ";
//            for (int i=0;i<timings.length();i++){
//                if(i>2){
//                    time=time+"...";
//                    break;
//                }
//                try {
//                    JSONObject temp = timings.getJSONObject(i);
//                    try {
//                        Date start = Config.timeFormat.parse(temp.getString("startTime"));
//                        Date finish = Config.timeFormat.parse(temp.getString("endTime"));
//                        time = time+setShortTime(start,finish)+" ";
//                    } catch (ParseException e) {
//                        e.printStackTrace();
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//            return time;
//        }
//    }
}
