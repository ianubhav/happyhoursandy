package com.viper.happyhours.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.viper.happyhours.DataModels.DateObjects;
import com.viper.happyhours.Fragment.DateSliderFragment;
import com.viper.happyhours.R;
import com.viper.happyhours.Utils.RealmTasks;

import java.util.ArrayList;

/**
 * Created by Viper on 07/04/16.
 */
public class DateAdapter extends PagerAdapter {

    private Context mContext;
    private ArrayList<DateObjects> dates;
    private LayoutInflater inflater;
    private int currentItemPos;
    private DateSliderFragment fragment;
    public DateAdapter(Context context, int resourceId, ArrayList<DateObjects> objects,DateSliderFragment fragment) {
        this.mContext = context;
        this.dates = objects;
        this.fragment = fragment;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setCurrentItem(int item) {
        this.currentItemPos = item;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        final ViewHolder holder;

        final DateObjects date = this.dates.get(position);

        View convertView = inflater.inflate(R.layout.date_box,container, false);
        holder = new ViewHolder();
        holder.dateTextView = (TextView) convertView.findViewById(R.id.layout_date_text);
        holder.dayTextview = (TextView) convertView.findViewById(R.id.layout_date_day);
        holder.outerLayout = (LinearLayout) convertView.findViewById(R.id.layout_date_item_outer_layout);
        convertView.setTag(Integer.valueOf(position));
        holder.dateTextView.setText(date.getDate());
        holder.dayTextview.setText(date.getDay());

        convertView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                fragment.onPagerItemClick(v, (Integer) v.getTag());
                RealmTasks.setSelectedDate(date.getDateObj());
//                setCurrentItem((Integer) v.getTag());
//                Log.d("Privatelog", dates.get((Integer) v.getTag()).getFormattedDate());
//                holder.outerLayout.setBackgroundColor(Color.parseColor("#e52e25"));
//                holder.dateTextView.setTextColor(Color.parseColor("#ffffff"));
//                holder.dayTextview.setTextColor(Color.parseColor("#ffffff"));

            }
        });

        if (position == currentItemPos) {
            holder.outerLayout.setBackgroundColor(Color.parseColor("#e52e25"));
            holder.dateTextView.setTextColor(Color.parseColor("#ffffff"));
            holder.dayTextview.setTextColor(Color.parseColor("#ffffff"));
        }


        ((ViewPager) container).addView(convertView);

        return convertView;
    }

    private class ViewHolder {
        private TextView dayTextview;
        private TextView dateTextView;
        private LinearLayout outerLayout;
    }

    @Override
    public int getCount() {
        return dates.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        // TODO Auto-generated method stub
        return view == (object);
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        ((ViewPager) container).removeView(view);
        view = null;
    }

    public float getPageWidth(int position) {
        return 0.2f;
    }

}