package com.viper.happyhours.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.AppCompatRadioButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.viper.happyhours.DataModels.OffersList;
import com.viper.happyhours.R;

import java.util.ArrayList;

/**
 * Created by Viper on 06/04/16.
 */
public class ListViewAdapterOthers extends ArrayAdapter<OffersList> {
    ArrayList<Integer> selectedItems = new ArrayList<Integer>();
    public void addSelected(int i){
        if(!selectedItems.contains(i)){
            selectedItems.add(i);
        }
    }
    public ArrayList<Integer> getArrayList(){
        return selectedItems;
    }
    public ArrayList<Integer> getArrayListId(){
        ArrayList<Integer> ids = new ArrayList<Integer>();
        for (int i=0;i<selectedItems.size();i++){
            ids.add(getItem(selectedItems.get(i)).getId());
        }
        return ids;
    }
    public void removeSelected(int i){
        selectedItems.remove((Integer) i );
    }
    public boolean containsSelected(int i){
        if(selectedItems.contains(i)){
            return true;
        }
        return false;
    }
    public ListViewAdapterOthers(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public ListViewAdapterOthers(Context context, int resource, ArrayList<OffersList> items) {
        super(context, resource, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.listview_others, null);
        }

        OffersList p = getItem(position);

        if (p != null) {
            RelativeLayout rl = (RelativeLayout) v.findViewById(R.id.listview_others_layout);
            AppCompatRadioButton rb = (AppCompatRadioButton) v.findViewById(R.id.radioButton_others);
            TextView actualPrice = (TextView) v.findViewById(R.id.listview_others_actual_price);
            TextView serviceName = (TextView) v.findViewById(R.id.listview_others_label);
            actualPrice.setText("Rs "+p.getDiscountFees());
            serviceName.setText(p.getService_name());
            if(selectedItems.contains(position)) {
                rb.setChecked(true);
                rl.setBackgroundColor(Color.parseColor("#DFDEDE"));
            }
            else {
                rb.setChecked(false);
                rl.setBackgroundColor(Color.parseColor("#ffffff"));
            }
        }

        return v;
    }

}
