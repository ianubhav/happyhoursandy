package com.viper.happyhours.DataModels;

import org.json.JSONArray;

/**
 * Created by Viper on 06/04/16.
 */
public class OffersList {
    private int id;
    private String service_name;
    private int actualFees;
    private int discountFees;
    private float percent;
    private JSONArray timings;

    public String getOfferType() {
        return offerType;
    }

    public void setOfferType(String offerType) {
        this.offerType = offerType;
    }

    private String offerType;

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getService_name() {
        return service_name;
    }
    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public int getActualFees() {
        return actualFees;
    }
    public void setActualFees(int actualFees) {
        this.actualFees = actualFees;
    }

    public int getDiscountFees() {
        return discountFees;
    }
    public void setDiscountFees(int discountFees) {
        this.discountFees = discountFees;
    }

    public float getPercent() {
        return percent;
    }
    public void setPercent(float percent) {
        this.percent = percent;
    }

    public JSONArray getTimings() {
        return timings;
    }
    public void setTimings(JSONArray timings) {
        this.timings = timings;
    }
}
