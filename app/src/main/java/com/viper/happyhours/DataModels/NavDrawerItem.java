package com.viper.happyhours.DataModels;


import android.support.v4.app.Fragment;

/**
 * Created by Viper on 26/03/16.
 */
public class NavDrawerItem {
    private String title;
    private int icon;
    private int count = 0;
    private boolean isCounterVisible = false;
//    private Fragment fragment;

    public NavDrawerItem(){}

    public NavDrawerItem(String title, int icon){
        this.title = title;
        this.icon = icon;
//        this.fragment = fragment;
    }

    public NavDrawerItem(String title, int icon,Fragment fragment, boolean isCounterVisible, int count){
        this.title = title;
        this.icon = icon;
        this.isCounterVisible = isCounterVisible;
        this.count = count;
//        this.fragment = fragment;
    }

    public void setTitle(String title){
        this.title = title;
    }
    public String getTitle(){
        return this.title;
    }

    public int getIcon(){
        return this.icon;
    }
    public void setIcon(int icon){
        this.icon = icon;
    }

    public int getCount(){
        return this.count;
    }
    public void setCount(int count){
        this.count = count;
    }

    public boolean getCounterVisibility(){
        return this.isCounterVisible;
    }
    public void setCounterVisibility(boolean isCounterVisible){
        this.isCounterVisible = isCounterVisible;
    }

}
