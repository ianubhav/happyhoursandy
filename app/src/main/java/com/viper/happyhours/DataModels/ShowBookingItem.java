package com.viper.happyhours.DataModels;

import org.json.JSONArray;

/**
 * Created by Viper on 22/04/16.
 */
public class ShowBookingItem {
    private int id;
    private String partner_name;
    private String tagline;
    private String type;
    private String service_lat;
    private String service_lng;
    private String partner_img;
    private String bookingActualTime;

    public String getBookingActualTime() {
        return bookingActualTime;
    }

    public void setBookingActualTime(String bookingActualTime) {
        this.bookingActualTime = bookingActualTime;
    }

    public String getBookingTime() {
        return bookingTime;
    }

    public void setBookingTime(String bookingTime) {
        this.bookingTime = bookingTime;
    }

    private String bookingTime;

    public void setTimings(JSONArray timings) {
        this.timings = timings;
    }

    private JSONArray timings;

    public String getTagline() {
        return tagline;
    }
    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getPartner_name() {
        return partner_name;
    }
    public void setPartner_name(String partner_name) {
        this.partner_name = partner_name;
    }

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    public String getService_lat() {
        return service_lat;
    }
    public void setService_lat(String service_lat) {
        this.service_lat = service_lat;
    }

    public String getService_lng() {
        return service_lng;
    }
    public void setService_lng(String service_lng) {
        this.service_lng = service_lng;
    }


    public String getPartner_img() {
        return partner_img;
    }
    public void setPartner_img(String partner_img) {
        this.partner_img = partner_img;
    }
}
