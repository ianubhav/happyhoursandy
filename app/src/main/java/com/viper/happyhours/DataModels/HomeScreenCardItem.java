package com.viper.happyhours.DataModels;

/**
 * Created by Viper on 19/03/16.
 */
public class HomeScreenCardItem {
    private int id;
    private String partner_name;
    private String tagline;
    private String type;
    private int max_discount;
    private String partner_img;
    private String timings;

    public String getTagline() {
        return tagline;
    }
    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getPartner_name() {
        return partner_name;
    }
    public void setPartner_name(String partner_name) {
        this.partner_name = partner_name;
    }

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    public String getPartner_img() {
        return partner_img;
    }
    public void setPartner_img(String partner_img) {
        this.partner_img = partner_img;
    }

    public String getTimings() {
        return timings;
    }
    public void setTimings(String timings) {
        this.timings = timings;
    }

    public int getMax_discount() {
        return max_discount;
    }

    public void setMax_discount(int max_discount) {
        this.max_discount = max_discount;
    }
}
