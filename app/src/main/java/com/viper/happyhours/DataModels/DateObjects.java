package com.viper.happyhours.DataModels;

import java.util.Date;

/**
 * Created by Viper on 07/04/16.
 */
public class DateObjects {
    private String date;
    private String day;
    private String formattedDate;
    private Date dateObj;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getFormattedDate() {
        return formattedDate;
    }

    public void setFormattedDate(String formattedDate) {
        this.formattedDate = formattedDate;
    }

    public Date getDateObj() {
        return dateObj;
    }

    public void setDateObj(Date dateObj) {
        this.dateObj = dateObj;
    }
}
