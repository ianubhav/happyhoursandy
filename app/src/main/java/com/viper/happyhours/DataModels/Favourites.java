package com.viper.happyhours.DataModels;

/**
 * Created by Viper on 14/03/16.
 */

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Favourites extends RealmObject {

    @PrimaryKey
    private Integer id;

    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getId() {
        return id;
    }
}