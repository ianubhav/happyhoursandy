package com.viper.happyhours.DataModels;

/**
 * Created by Viper on 14/03/16.
 */

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

;

// Your model just have to extend RealmObject.
// This will inherit an annotation which produces proxy getters and setters for all fields.
public class User extends RealmObject {

    // All fields are by default persisted.
    private String f_name;
    private String l_name;

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    private int user_id;
    private String number;
    private String email;
    private String gender;
    private Double default_latitude;
    private Double default_longitude;
    private Double current_latitude;
    private Double current_longitude;
    private String os;
    private String os_version;
    private String device_id;
    private String access_token;
    private String location_text;
    private Date selectedDate;
    @PrimaryKey
    private Integer realm_id;
    // Other objects in a one-to-one relation must also subclass RealmObject

    public Date getSelectedDate() {
        return selectedDate;
    }
    public void setSelectedDate(Date selectedDate) {
        this.selectedDate = selectedDate;
    }


    public Integer getRealm_id() {
        return realm_id;
    }
    public void setRealm_id(Integer realm_id) {
        this.realm_id = realm_id;
    }


    public String getF_name() {
        return f_name;
    }
    public void setF_name(String f_name) {
        this.f_name = f_name;
    }

    public String getL_name() {
        return l_name;
    }
    public void setL_name(String l_name) {
        this.l_name = l_name;
    }

//    public String getFullName(){
//        return f_name+" "+l_name;
//    }

    public String getGender() {
        return gender;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getOs() {
        return os;
    }
    public void setOs(String os) {
        this.os = os;
    }

    public String getNumber() {
        return number;
    }
    public void setNumber(String number) {
        this.number = number;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public Double getCurrent_latitude() {
        return current_latitude;
    }
    public void setCurrent_latitude(Double current_latitude) {
        this.current_latitude = current_latitude;
    }

    public Double getCurrent_longitude() {
        return current_longitude;
    }
    public void setCurrent_longitude(Double current_longitude) {
        this.current_longitude = current_longitude;
    }

    public String getDevice_id() {
        return device_id;
    }
    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getOs_version() {
        return os_version;
    }
    public void setOs_version(String os_version) {
        this.os_version = os_version;
    }

    public String getAccess_token() {
        return access_token;
    }
    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public Double getDefault_latitude() {
        return default_latitude;
    }
    public void setDefault_latitude(Double default_latitude) {
        this.default_latitude = default_latitude;
    }

    public Double getDefault_longitude() {
        return default_longitude;
    }
    public void setDefault_longitude(Double default_longitude) {
        this.default_longitude = default_longitude;
    }

    public String getLocation_text() {
        return location_text;
    }
    public void setLocation_text(String location_text) {
        this.location_text = location_text;
    }
}