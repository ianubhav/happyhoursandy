package com.viper.happyhours.DataModels;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Viper on 15/03/16.
 */
public class PartnerObjects extends RealmObject{

    @PrimaryKey
    private int id;
    private String partner_name;
    private String type;
    private String service_lat;
    private String service_lng;
    private int min_value;
    private String service_img;
    private String service_name;
    private String time;
    private String fees;
    private String discount_percent;
    private String discount_absolute;
    private String service_status;
    private String comment;
    private String service_by;
    private int rating;

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getPartner_name() {
        return partner_name;
    }
    public void setPartner_name(String partner_name) {
        this.partner_name = partner_name;
    }

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    public String getService_lat() {
        return service_lat;
    }
    public void setService_lat(String service_lat) {
        this.service_lat = service_lat;
    }

    public String getService_lng() {
        return service_lng;
    }
    public void setService_lng(String service_lng) {
        this.service_lng = service_lng;
    }

    public int getMin_value() {
        return min_value;
    }
    public void setMin_value(int min_value) {
        this.min_value = min_value;
    }

    public String getService_img() {
        return service_img;
    }
    public void setService_img(String service_img) {
        this.service_img = service_img;
    }

    public String getService_name() {
        return service_name;
    }
    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public String getTime() {
        return time;
    }
    public void setTime(String time) {
        this.time = time;
    }

    public String getFees() {
        return fees;
    }
    public void setFees(String fees) {
        this.fees = fees;
    }

    public String getDiscount_percent() {
        return discount_percent;
    }
    public void setDiscount_percent(String discount_percent) {
        this.discount_percent = discount_percent;
    }

    public String getDiscount_absolute() {
        return discount_absolute;
    }
    public void setDiscount_absolute(String discount_absolute) {
        this.discount_absolute = discount_absolute;
    }

    public String getService_status() {
        return service_status;
    }
    public void setService_status(String service_status) {
        this.service_status = service_status;
    }

    public String getComment() {
        return comment;
    }
    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getService_by() {
        return service_by;
    }
    public void setService_by(String service_by) {
        this.service_by = service_by;
    }

    public int getRating() {
        return rating;
    }
    public void setRating(int rating) {
        this.rating = rating;
    }
}