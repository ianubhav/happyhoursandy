package com.viper.happyhours.DataModels;

import java.util.Date;

import io.realm.RealmObject;

/**
 * Created by Viper on 13/04/16.
 */
public class ExtraData extends RealmObject {
    private Date selectedDate;

    public Date getSelectedDate() {
        return selectedDate;
    }
    public void setSelectedDate(Date selectedDate) {
        this.selectedDate = selectedDate;
    }
}
