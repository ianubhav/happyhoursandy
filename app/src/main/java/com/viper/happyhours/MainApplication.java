package com.viper.happyhours;

import android.app.Application;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.viper.happyhours.Utils.LruBitmapCache;
import com.viper.happyhours.Utils.RealmTasks;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Viper on 14/03/16.
 */
public class MainApplication extends Application {

    public static final String TAG = MainApplication.class.getSimpleName();
    private RequestQueue mRequestQueue;
    private static MainApplication mInstance;
    private ImageLoader mImageLoader;
    @Override
    public void onCreate() {
        super.onCreate();
        RealmConfiguration config = new RealmConfiguration.Builder(getApplicationContext()).build();
        Realm.setDefaultConfiguration(config);
        RealmTasks.setLocationInUserObject(null,null,null,getApplicationContext(),true);
        mInstance = this;

    }
    public static synchronized MainApplication getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue, new LruBitmapCache());
        }
        Log.d("MainApplication", "MainApplication: getImageLoader");
        return this.mImageLoader;
    }


    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

}
