package com.viper.happyhours.Fragment;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lsjwzh.widget.materialloadingprogressbar.CircleProgressBar;
import com.viper.happyhours.Adapters.HomeScreenViewPagerAdapter;
import com.viper.happyhours.R;
import com.viper.happyhours.Utils.Config;

public class HomeFragment extends Fragment {
    public static final String ARG_PAGE = "ARG_PAGE";
    private TabLayout tabLayout;
    private CircleProgressBar circleProgressBar;
    private String[] tabs = Config.homeScreenTabs;
    private ViewPager viewPager;
    public HomeFragment(){

    }
//    public static HomeFragment newInstance(String type) {
//        Bundle args = new Bundle();
//        args.putString(ARG_PAGE, type);
//        HomeFragment fragment = new HomeFragment();
//        fragment.setArguments(args);
//        return fragment;
//    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
//        Log.d("Privatelog", "HomeFragment: onCreateView");
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        tabLayout = (TabLayout) view.findViewById(R.id.home_fragment_tablayout);
        circleProgressBar = (CircleProgressBar) view.findViewById(R.id.progress_bar_home_fragment);
        viewPager = (ViewPager) view.findViewById(R.id.home_fragment_viewpager);
        for (String tab: tabs){
            tabLayout.addTab(tabLayout.newTab().setText(tab));
        }
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        viewPager.setAdapter(new HomeScreenViewPagerAdapter(getChildFragmentManager(),tabLayout.getTabCount()));
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPager.setOffscreenPageLimit(tabLayout.getTabCount());
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        DateSliderFragment dateSliderFragment = new DateSliderFragment();
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.add(R.id.home_fragment_date_changer_frame, dateSliderFragment);
        transaction.commit();
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        Log.d("Privatelog", "HomeFragment: onDestroy");
    }
}
