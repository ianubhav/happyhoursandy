package com.viper.happyhours.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.viper.happyhours.Adapters.DateAdapter;
import com.viper.happyhours.DataModels.DateObjects;
import com.viper.happyhours.R;
import com.viper.happyhours.Utils.Config;
import com.viper.happyhours.Utils.Extras;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class DateSliderFragment extends Fragment {
    private ViewPager viewPager;
    private DateAdapter adapter;
    public DateSliderFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_date_slider, container, false);
        viewPager = (ViewPager) view.findViewById(R.id.date_viewpager);
        adapter = new DateAdapter(getActivity(), R.layout.date_box, getDates(),this);
        adapter.setCurrentItem(0);
        viewPager.setAdapter(adapter);

//        onPagerItemClick(viewPager.getChildAt(0), 0);
        // set intial position.
//        viewPager.onPagerItemClick(pager.getChildAt(0), 0);
        return view;
    }

    private ArrayList<DateObjects> getDates() {
        ArrayList<DateObjects> dates = new ArrayList<DateObjects>();
        Calendar calendar = Calendar.getInstance();
        for (int index = 1; index < Config.maxDates; index++) {

            DateObjects date = new DateObjects();
            date.setDateObj(calendar.getTime());
            date.setDate("" + calendar.get(Calendar.DATE));
            date.setDay(Extras.getDay(calendar.get(Calendar.DAY_OF_WEEK)));
            date.setFormattedDate(calendar.get(Calendar.DATE)+"-" + calendar.get(Calendar.MONTH) + "-" +  calendar.get(Calendar.YEAR));

            dates.add(date);
            calendar.add(Calendar.DATE, 1);

        }
        return dates;
    }
    public void onPagerItemClick(View view, int index) {
        adapter.setCurrentItem(index);
        viewPager.setAdapter(adapter);
        Log.d("Privatelog", String.valueOf(index));
    }

}
