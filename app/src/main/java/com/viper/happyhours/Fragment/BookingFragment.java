package com.viper.happyhours.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.viper.happyhours.Adapters.ShowBookingAdapter;
import com.viper.happyhours.R;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class BookingFragment extends Fragment {
    private RecyclerView mRecycler;
    private ShowBookingAdapter mAdapter;
    private LinearLayoutManager llm;
    Map<String, String> params = new HashMap<String, String>();

    public BookingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_booking, container, false);
        mRecycler = (RecyclerView) view.findViewById(R.id.viewpager_recycler_view);
        llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mRecycler.setLayoutManager(llm);
        return view;
    }

}
