package com.viper.happyhours.Fragment;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.gc.materialdesign.views.ButtonRectangle;
import com.google.gson.Gson;
import com.viper.happyhours.Activity.PartnerHomeActivity;
import com.viper.happyhours.Adapters.BookingListAdapter;
import com.viper.happyhours.DataModels.OffersList;
import com.viper.happyhours.R;
import com.viper.happyhours.Utils.ApiRequests;
import com.viper.happyhours.Utils.Config;
import com.viper.happyhours.Utils.RealmTasks;
import com.viper.happyhours.Utils.VolleyResponseListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class BookingDialogFragment extends DialogFragment {
    private ArrayList<ArrayList<OffersList>> list = PartnerHomeActivity.arrayList;
    private TextView bookingMain;
    private TextView date;
    private Button time;
    private ListView listView;
    private TextView title,extras;
    private ButtonRectangle button;
    private Map<String, String> params = new HashMap<String, String>();
    private SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");
    public static String timeSelected = null;
    public BookingDialogFragment() {

    }

    public static BookingDialogFragment newInstance() {
        BookingDialogFragment frag = new BookingDialogFragment();
        timeSelected = null;
//        Log.d("Privatelogplus", PartnerHomeActivity.timing);
        return frag;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_booking_dialog, container);
        final ProgressDialog pDialog = new ProgressDialog(view.getContext());
        Log.d("Privatelog", view.getWidth() + " " + view.getHeight());
        getDialog().getWindow().setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP);
        getDialog().getWindow().setLayout(4000, 5000);
        Intent i = getActivity().getIntent();
        final int partnerId = i.getExtras().getInt("partnerId");
        title = (TextView) view.findViewById(R.id.booking_partner_name);
        bookingMain =(TextView) view.findViewById(R.id.booking_main_text);
        date = (TextView) view.findViewById(R.id.booking_date);
        extras = (TextView) view.findViewById(R.id.booking_extra_text);
        time = (Button) view.findViewById(R.id.booking_time_button);
        button = (ButtonRectangle) view.findViewById(R.id.booking_button);
        listView = (ListView) view.findViewById(R.id.booking_list);
        title.setText(PartnerHomeActivity.title);
        if(PartnerHomeActivity.offerId>-1) {
            time.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final CharSequence[] sequence = setTime(PartnerHomeActivity.arrayList.get(0).get(PartnerHomeActivity.offerId).getTimings());
                    final AlertDialog.Builder alt_bld = new AlertDialog.Builder(getActivity());
                    alt_bld.setTitle("Select a Time");
                    alt_bld.setSingleChoiceItems(sequence, -1, new DialogInterface
                            .OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {
                            timeSelected = (String) sequence[item];
                            time.setText(timeSelected);
                            dialog.dismiss();
                        }
                    });
                    AlertDialog alert = alt_bld.create();
                    alert.show();
                }
            });
        }
        else{
            time.setClickable(false);
            timeSelected = "Any Time";
            time.setText(timeSelected);
        }
        date.setText("Date: "+format.format(RealmTasks.getSelectedDate()));
        BookingListAdapter adapter = new BookingListAdapter(getActivity().getBaseContext(),R.layout.booking_list_item,getItems());
        listView.setAdapter(adapter);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(timeSelected!=null) {
                    pDialog.show();
                    params.put("customer_id", String.valueOf(RealmTasks.getCustomerId()));
                    params.put("partner_id", String.valueOf(partnerId));
                    params.put("time",timeSelected);
                    params.put("date", Config.dateFormat.format(RealmTasks.getSelectedDate()));
                    params.put("data", setJsonData().toString());
//                params.put("time", PartnerHomeActivity.timing);
                    ApiRequests.makeJsonObjectRequest(Request.Method.POST, view.getContext(), Config.makeBooking, params, new VolleyResponseListener() {
                        @Override
                        public void onError(String message) {
                            pDialog.dismiss();
                        }

                        @Override
                        public void onResponse(Object response) {
                            JSONObject data = (JSONObject) response;
                            try {
                                if(!data.getBoolean("is_error")){
                                    bookingMain.setText("Booking id: " + data.getString("id"));
                                    button.setVisibility(View.GONE);
                                    extras.setVisibility(View.VISIBLE);
                                    time.setClickable(false);
                                }
                                Toast.makeText(view.getContext(), data.getString("message"), Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            pDialog.dismiss();
                        }
                    });
                }
                else {
                    Toast.makeText(view.getContext(),"Please Select A Time Slot",Toast.LENGTH_SHORT).show();
                }
            }
        });
        return view;
    }
    private ArrayList<OffersList> getItems(){
        ArrayList<OffersList> items = new ArrayList<OffersList>();
        if(PartnerHomeActivity.offerId>-1) {
            Log.d("Privatelogand", list.get(0).get(PartnerHomeActivity.offerId).getService_name());
            items.add(list.get(0).get(PartnerHomeActivity.offerId));
        }
        for (int i=0;i<PartnerHomeActivity.otherIds.size();i++){
            items.add(list.get(1).get(i));
        }
        OffersList total = new OffersList();
        total.setService_name("Total");
        int a=0;
        for (int i=0;i<items.size();i++){
            a=a+items.get(i).getDiscountFees();
        }
        total.setDiscountFees(a);
        items.add(total);
        Log.d("Privatelogpj", String.valueOf(items.get(0).getService_name()));
        return items;
    }
    private String setJsonData(){
        return new Gson().toJson(getItems());
    }
    private CharSequence[] setTime(JSONArray jsonArray){
        CharSequence[] sequences = new String[jsonArray.length()];
        for(int i=0;i<jsonArray.length();i++){
            try {
                sequences[i] = jsonArray.getString(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return sequences;
    }

}