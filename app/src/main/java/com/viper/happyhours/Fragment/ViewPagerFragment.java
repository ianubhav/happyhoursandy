package com.viper.happyhours.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.lsjwzh.widget.materialloadingprogressbar.CircleProgressBar;
import com.viper.happyhours.Activity.PartnerHomeActivity;
import com.viper.happyhours.Adapters.CardViewAdapter;
import com.viper.happyhours.DataModels.HomeScreenCardItem;
import com.viper.happyhours.R;
import com.viper.happyhours.Utils.ApiRequests;
import com.viper.happyhours.Utils.Config;
import com.viper.happyhours.Utils.RealmTasks;
import com.viper.happyhours.Utils.VolleyResponseListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ViewPagerFragment extends Fragment {
    Map<String, String> params = new HashMap<String, String>();
    private CircleProgressBar circleProgressBar;
    private String TAG = "ViewPagerFragment";
    private RecyclerView mRecycler;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private CardViewAdapter mAdapter;
    private ArrayList<HomeScreenCardItem> cardItemArrayList;
    private LinearLayoutManager llm;
    private String type;
    private TextView emptyView;

    public static ViewPagerFragment newInstance(String type) {
        ViewPagerFragment f = new ViewPagerFragment();
        f.type = type;
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d("Privatelog", "ViewPagerFragment: onCreateView   " + type);
        View view = inflater.inflate(R.layout.fragment_viewpager, container, false);
        emptyView = (TextView) view.findViewById(R.id.viewpager_empty_view);
        mRecycler = (RecyclerView) view.findViewById(R.id.viewpager_recycler_view);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.viewpager_swipe_refresh_layout);
        llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mRecycler.setLayoutManager(llm);
        mRecycler.addOnItemTouchListener(new RecyclerTouchListener(getActivity().getBaseContext(), mRecycler, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                HomeScreenCardItem cardItem  =cardItemArrayList.get(position);
                Intent i = new Intent(getActivity(), PartnerHomeActivity.class);
                i.putExtra("id", cardItem.getId());
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                setRecyclerView();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
        setRecyclerView();
        return view;
    }

    private ArrayList<HomeScreenCardItem> setServicesList(JSONObject jsondata){
        ArrayList<HomeScreenCardItem> finalList = new ArrayList<HomeScreenCardItem>();
        JSONArray data = null;
        try {
            data = jsondata.getJSONArray("data");
            if(data.length()==0){
                emptyView.setVisibility(View.VISIBLE);
                mSwipeRefreshLayout.setVisibility(View.GONE);
                return finalList;
            }
            for (int i = 0; i < data.length(); i++) {
                try {
                    JSONObject row = data.getJSONObject(i);
                    HomeScreenCardItem cardItem = new HomeScreenCardItem();
                    cardItem.setId(row.getInt("id"));
                    cardItem.setPartner_name(row.getString("businessName"));
                    cardItem.setType(row.getString("serviceType"));
                    cardItem.setTagline(row.getString("tagLine"));
                    if(!row.isNull("max_discount")){
                        cardItem.setMax_discount(row.getInt("max_discount"));
                    }
                    cardItem.setPartner_img(row.getString("partnerImgUrl"));
                    cardItem.setTimings(row.getString("dateTime"));
                    finalList.add(cardItem);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();

        }

        return finalList;
    }
    public void setRecyclerView(){
        params.putAll(RealmTasks.getLocationParams());
        params.put("type", type);
        params.put("token",RealmTasks.getToken());
        params.put("date", Config.dateFormat.format(RealmTasks.getSelectedDate()));
        params.putAll(RealmTasks.getLocationParams());
        ApiRequests.makeJsonObjectRequest(Request.Method.POST, getActivity(), Config.partnerList, params, new VolleyResponseListener() {
            @Override
            public void onError(String message) {
                Log.d("hello", "onError() called with: " + "message = [" + message + "]");
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onResponse(Object response) {
                JSONObject jsonData = (JSONObject) response;
                cardItemArrayList = setServicesList(jsonData);
                mAdapter = new CardViewAdapter(cardItemArrayList);
                mRecycler.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
                mAdapter.setOnItemClickListener(new CardViewAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(int id) {
                        Intent i = new Intent(getActivity(), PartnerHomeActivity.class);
                        i.putExtra("partnerId", id);
                        startActivity(i);
                        getActivity().overridePendingTransition(R.transition.enter, R.transition.exit);
                    }
                });
//                circleProgressBar.setVisibility(View.GONE);
            }
        });
    }
    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ViewPagerFragment.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ViewPagerFragment.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
