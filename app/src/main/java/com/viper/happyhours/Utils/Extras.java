package com.viper.happyhours.Utils;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;

import io.realm.Realm;

/**
 * Created by Viper on 14/03/16.
 */
public class Extras {

    public static void copy(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        OutputStream out = new FileOutputStream(dst);

        // Transfer bytes from in to out
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }
    public static void pushRealm(Realm realm,String fileName){
        try {
            File f = new File(realm.getPath());
            if (f.exists()) {
                try {
                    Log.d("pushRealm: ",Environment.getExternalStorageDirectory()+"/"+fileName);
                    Extras.copy(f, new File(Environment.getExternalStorageDirectory()+"/"+fileName));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } finally {
            if (realm != null)
                realm.close();
        }
    }

    public static boolean isValidMobile(String phoneNo) {
        return phoneNo.matches("^([7-9]{1})([0-9]{9})|0([7-9]{1})([0-9]{9})$");
    }
    public static String removeZeroMobileNo(String phoneNo) {
        return phoneNo.replaceFirst("^0+(?!$)", "");
    }

    public static void jsonError(Context context){
        Log.e("PrivateError", "Json Error");
    }

    public static void internetConnectionToast(Context context,String message){
        Log.e("PrivateError", message);
        Toast.makeText(context, "Please Check Your Internet Connection", Toast.LENGTH_SHORT).show();
    }

    public static String getDay(int index) {
        switch (index) {
            case Calendar.SUNDAY:
                return "SUN";
            case Calendar.MONDAY:
                return "MON";
            case Calendar.TUESDAY:
                return "TUE";
            case Calendar.WEDNESDAY:
                return "WED";
            case Calendar.THURSDAY:
                return "THUR";
            case Calendar.FRIDAY:
                return "FRI";
            case Calendar.SATURDAY:
                return "SAT";
        }
        return "";
    }
//    private String getMonth(int index) {
//        switch (index) {
//            case Calendar.JANUARY:
//                return "JAN";
//            case Calendar.FEBRUARY:
//                return "FEB";
//            case Calendar.MARCH:
//                return "MAR";
//            case Calendar.APRIL:
//                return "APR";
//            case Calendar.MAY:
//                return "MAY";
//            case Calendar.JUNE:
//                return "JUN";
//            case Calendar.JULY:
//                return "JUL";
//            case Calendar.AUGUST:
//                return "AUG";
//            case Calendar.SEPTEMBER:
//                return "SEP";
//            case Calendar.OCTOBER:
//                return "OCT";
//            case Calendar.NOVEMBER:
//                return "NOV";
//            case Calendar.DECEMBER:
//                return "DEC";
//        }
//        return "";
//    }
}
