package com.viper.happyhours.Utils;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.viper.happyhours.DataModels.NavDrawerItem;
import com.viper.happyhours.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by Viper on 14/03/16.
 */
public class Config {
    public static final String ApiRequestUrl = "http://192.168.0.2:8000/api/";
//    public static final String ApiRequestUrl = "http://54.210.206.133:8000/api/";
    public static final String userLogin = ApiRequestUrl+"userlogin";
    public static final String signUpUserUrl = ApiRequestUrl+"usersignup";
    public static final String userOtpVerifyUrl = ApiRequestUrl+"verifyotp";
    public static final String updateUserUrl = ApiRequestUrl+"updateuser";
    public static final String logoutUserUrl = ApiRequestUrl+"logoutuser";
    public static final String partnerList = ApiRequestUrl+"partnerlist";
    public static final String partnerHome = ApiRequestUrl+"partnerhome";
    public static final String makeBooking = ApiRequestUrl+"makebooking";
    public static final String getBooking = ApiRequestUrl+"getbookings";
    public static final String[] homeScreenTabs = { "salon", "gym", "party" };
    public static final int maxDates = 15;
    public static final String mapsApiKey = "AIzaSyDKb-CHr3yov8fhMeNIWUuDP53NLwoLN2k";
    public static final LatLngBounds latLngBounds = new LatLngBounds( new LatLng(9.756740, 73.442857), new LatLng(32.957089, 78.540514));
    public static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    public static final SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm:ss");
    public static final ArrayList<NavDrawerItem> sideMenuTabs = setSideMenuTabs();

    public static ArrayList<NavDrawerItem> setSideMenuTabs(){
        ArrayList<NavDrawerItem> navDrawerItems = new ArrayList<NavDrawerItem>();
        navDrawerItems.add(new NavDrawerItem("Home",R.drawable.ic_home_black_24dp));
//        navDrawerItems.add(new NavDrawerItem("Notifications",R.drawable.ic_notifications_none_black_24dp, new Notifications()));
        navDrawerItems.add(new NavDrawerItem("Bookings",R.drawable.ic_event_black_24dp));
//        navDrawerItems.add(new NavDrawerItem("Favourites",R.drawable.ic_favorite_border_black_24dp,new FavouritesFragment()));
//        navDrawerItems.add(new NavDrawerItem("About Us",R.drawable.ic_info_outline_black_24dp,new AboutUsFragment()));
//        navDrawerItems.add(new NavDrawerItem("Help",R.drawable.ic_help_outline_black_24dp,new HelpFragment()));
        navDrawerItems.add(new NavDrawerItem("Logout",R.drawable.logout_icon));
        return navDrawerItems;
    }


}
