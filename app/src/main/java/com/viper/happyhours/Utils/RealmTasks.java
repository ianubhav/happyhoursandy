package com.viper.happyhours.Utils;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.viper.happyhours.Activity.LoginScreen;
import com.viper.happyhours.DataModels.Favourites;
import com.viper.happyhours.DataModels.PartnerObjects;
import com.viper.happyhours.DataModels.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;

/**
 * Created by Viper on 15/03/16.
 */
public class RealmTasks {
    public static void updateUserObject(final JSONObject jsonObject, final Context context){
        try {
            final JSONObject data = jsonObject.getJSONObject("data");
            final Realm realm = Realm.getDefaultInstance();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm bgRealm) {
                    User user = new User();
                    try {
                        Log.d("Privatelog", "executing Realm transaction for updating user object ");
                        user.setRealm_id(1);
                        user.setNumber(data.getString("mobileNo"));
                        user.setF_name(data.getString("firstName"));
                        user.setL_name(data.getString("lastName"));
                        user.setAccess_token(jsonObject.getString("token"));
                        user.setUser_id(data.getInt("id"));
                        if(!data.isNull("defaultlat") ) {
                            user.setDefault_latitude(data.getDouble("defaultLat"));
                            user.setDefault_longitude(data.getDouble("defaultLng"));
                        }
                        bgRealm.copyToRealmOrUpdate(user);
                    } catch (JSONException e) {
                        Extras.jsonError(context);
                        e.printStackTrace();
                    }
                }
            }, null);
        } catch (JSONException e) {
            Extras.jsonError(context);
            e.printStackTrace();
        }

    }

    public static void logoutUser(final Context context){
        final Map<String, String> params = new HashMap<String, String>();
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                // begin & end transcation calls are done for you
                User user = realm.where(User.class).equalTo("realm_id", 1).findFirst();
                params.put("token", user.getAccess_token());
                user.removeFromRealm();
            }
        }, new Realm.Transaction.Callback() {
            @Override
            public void onSuccess() {
                Intent loginscreen = new Intent(context, LoginScreen.class);
                loginscreen.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(loginscreen);
//                ((Activity) (context)).finish();
                Toast.makeText(context, "Successfully logged out", Toast.LENGTH_SHORT);
                ApiRequests.makeJsonObjectRequest(Request.Method.POST,context, Config.logoutUserUrl, params, new VolleyResponseListener() {
                    @Override
                    public void onError(String message) {
                        Log.e("Logout Error", message );
                    }

                    @Override
                    public void onResponse(Object response) {

                    }
                });
            }
        });
    }
    public static boolean loginStatus(){
        Realm realm = Realm.getDefaultInstance();
        User user = realm.where(User.class).equalTo("realm_id", 1).findFirst();
        if (user != null){
            return true;
        }
        return false;
    }
    public static boolean isCurrentLocationAvailable(){
        Realm realm = Realm.getDefaultInstance();
        User user = realm.where(User.class).equalTo("realm_id", 1).findFirst();
        if(user != null){
            if(!(user.getCurrent_longitude() ==null) & !(user.getCurrent_longitude() ==null)){
                return true;
            }
        }
        return false;
    }

    public static void setLocationInUserObject(final Double latitude, final Double longitude, final String locationText, final Context context, final Boolean forClear){
        Log.d("Privateloglati", String.valueOf(latitude));
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm bgRealm) {
                User user = bgRealm.where(User.class).equalTo("realm_id", 1).findFirst();
                user.setCurrent_latitude(latitude);
                user.setCurrent_longitude(longitude);
                user.setLocation_text(locationText);
                bgRealm.copyToRealmOrUpdate(user);
            }
        }, new Realm.Transaction.Callback() {
            @Override
            public void onSuccess() {
                // Transaction was a success.
                Log.d("realmobject", "Transaction was a success." );
                if(!forClear) {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("token", getToken());
                    params.put("current_lat", String.valueOf(latitude));
                    params.put("current_lng", String.valueOf(longitude));
                    ApiRequests.makeJsonObjectRequest(Request.Method.POST, context, Config.updateUserUrl, params, new VolleyResponseListener() {
                        @Override
                        public void onError(String message) {
                        }

                        @Override
                        public void onResponse(Object response) {
                        }
                    });
                }
            }
        });
    }

    public static void setServicesList(JSONObject object,Context mContext){
        try {
            final JSONArray data = object.getJSONObject("data").getJSONArray("data");
            final Realm realm = Realm.getDefaultInstance();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm bgRealm) {
                    bgRealm.clear(PartnerObjects.class);
                    for (int i=0;i<data.length();i++){
                        try {
                            JSONObject row = data.getJSONObject(i);
                            JSONObject service = row.getJSONObject("services");
                            JSONObject details = service.getJSONObject("details");
                            PartnerObjects partnerObjects = new PartnerObjects();
                            partnerObjects.setId(row.getInt("id"));
                            partnerObjects.setPartner_name(row.getString("partner_id"));
                            partnerObjects.setType(row.getString("type"));
                            partnerObjects.setService_lat(row.getString("service_lat"));
                            partnerObjects.setService_lng(row.getString("service_lng"));
                            partnerObjects.setService_name(service.getString("service_name"));
                            partnerObjects.setService_img(service.getString("service_img"));
                            partnerObjects.setTime(details.getString("comment"));
                            partnerObjects.setFees(details.getString("fees"));
                            partnerObjects.setDiscount_absolute(details.getString("discount_absolute"));
                            partnerObjects.setDiscount_percent(details.getString("discount_percent"));
                            partnerObjects.setService_status(details.getString("service_status"));
                            partnerObjects.setService_by(details.getString("service_by"));
                            partnerObjects.setComment(details.getString("comment"));
//                            partnerObjects.setRating(details.getInt("ratig"));
                            bgRealm.copyToRealmOrUpdate(partnerObjects);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, new Realm.Transaction.Callback() {
                @Override
                public void onSuccess() {
                    // Transaction was a success.
                    Log.d("realmobject", "Partner was a success." );

                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public static  Map<String, String> getLocationParams(){
        Map<String, String> params = new HashMap<String, String>();
        Realm realm =Realm.getDefaultInstance();
        realm.beginTransaction();
        User user = realm.where(User.class).equalTo("realm_id", 1).findFirst();
        realm.commitTransaction();
        params.put("lat", String.valueOf(user.getCurrent_latitude()));
        params.put("lng", String.valueOf(user.getCurrent_longitude()));

        return params;
    }
    public static String getLocationText(){
        Realm realm =Realm.getDefaultInstance();
        realm.beginTransaction();
        User user = realm.where(User.class).equalTo("realm_id", 1).findFirst();
        realm.commitTransaction();
        return user.getLocation_text();
    }
    public static String getToken(){
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        User user = realm.where(User.class).equalTo("realm_id", 1).findFirst();
        realm.commitTransaction();
        return user.getAccess_token();
    }
    public static int getCustomerId(){
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        User user = realm.where(User.class).equalTo("realm_id", 1).findFirst();
        realm.commitTransaction();
        return user.getUser_id();
    }
    public static void setSelectedDate(Date date){
        Realm realm =Realm.getDefaultInstance();
        realm.beginTransaction();
        User user = realm.where(User.class).equalTo("realm_id", 1).findFirst();
        user.setSelectedDate(date);
        realm.commitTransaction();
    }
    public static Date getSelectedDate(){
        Realm realm =Realm.getDefaultInstance();
        realm.beginTransaction();
        User user = realm.where(User.class).equalTo("realm_id", 1).findFirst();
        realm.commitTransaction();
        return user.getSelectedDate();
    }
//    Not Used Now
    public static void createUserObject(final Context context){
            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            User user = new User();
            user.setRealm_id(1);
            realm.commitTransaction();
    }
    public static void setFavourite(int id){
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        Favourites favourites = realm.where(Favourites.class).equalTo("id", id).findFirst();
        if(favourites==null){
            favourites = new Favourites();
            favourites.setId(id);
        }
        else {
            favourites.removeFromRealm();
        }
        realm.commitTransaction();
    }
    public static boolean getFavourite(int id){
        Realm realm =Realm.getDefaultInstance();
        realm.beginTransaction();
        Favourites favourites = realm.where(Favourites.class).equalTo("id", id).findFirst();
        realm.commitTransaction();
        if(favourites==null){
            return false;
        }
        return true;
    }
    public static String getMobileNo(){
        Realm realm =Realm.getDefaultInstance();
        realm.beginTransaction();
        User user = realm.where(User.class).equalTo("realm_id", 1).findFirst();
        realm.commitTransaction();
        return user.getNumber();
    }
}
