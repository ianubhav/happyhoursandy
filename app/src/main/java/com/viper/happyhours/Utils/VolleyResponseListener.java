package com.viper.happyhours.Utils;

/**
 * Created by Viper on 15/03/16.
 */
public interface VolleyResponseListener {
    void onError(String message);
    void onResponse(Object response);
}
