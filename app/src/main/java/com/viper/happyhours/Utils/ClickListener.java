package com.viper.happyhours.Utils;

import android.view.View;

/**
 * Created by Viper on 04/04/16.
 */
public interface ClickListener {
    void onClick(View view, int position);
    void onLongClick(View view, int position);
}


